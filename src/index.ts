import './style.css'
import { twoSum } from './leetcode'

// console.log('Hello, World!')
console.log(twoSum([2, 7, 11, 15], 9))
console.log(twoSum([3, 2, 4], 6))

if (module.hot !== undefined) {
  module.hot.accept()
}
